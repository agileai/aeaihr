package com.agileai.hr.module.information.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.annotation.Validated;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataBox;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.HttpClientHelper;
import com.agileai.hotweb.controller.core.MasterSubListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.hr.module.information.service.HrEmployeeManage;
import com.agileai.util.StringUtil;

public class HrEmployeeManageListHandler
        extends MasterSubListHandler {
    public HrEmployeeManageListHandler() {
        super();
        this.editHandlerClazz = HrEmployeeManageEditHandler.class;
        this.serviceId = buildServiceId(HrEmployeeManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
	
 		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		String empWorkState = param.get("empWorkState");
		String empState = param.get("EMP_STATE");
		if("01".equals(empWorkState)){
			param.put("dimission","Y");
		}else{
			param.put("undimission","Y");
		}
		
		if(StringUtil.isNullOrEmpty(empState)) {
			param.put("EMP_STATE","approved");
		}
		
		List<DataRow> rsList = getService().findMasterRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("empSex",
                     FormSelectFactory.create("USER_SEX")
                                      .addSelectedValue(param.get("empSex")));
        setAttribute("EMP_STATE", FormSelectFactory.create("EMP_STATE")
									  .addSelectedValue(param.get("EMP_STATE")).addHasBlankValue(false));
        setAttribute("EMP_IS_SYNC",
                FormSelectFactory.create("EMP_IS_SYNC")
                                 .addSelectedValue(param.get("IS_SYNC")));
        initMappingItem("EMP_IS_SYNC",
                        FormSelectFactory.create("IS_SYNC").getContent());
        
        initMappingItem("EMP_SEX",
                FormSelectFactory.create("USER_SEX").getContent());
        
        initMappingItem("EMP_EDUCATION",
                        FormSelectFactory.create("EMP_EDUCATION").getContent());
        initMappingItem("EMP_STATE",
                FormSelectFactory.create("EMP_STATE").getContent());
        initMappingItem("EMP_WORK_STATE",
        		FormSelectFactory.create("EMP_WORK_STATE").getContent());
		setAttribute("empWorkState", buildEmpWorkStateSelect().addSelectedValue(param.get("empWorkState","00")));
		
    }
    
	
    
    public ViewRenderer doSynchronizationAction(DataParam param){
    	String responseBody = FAIL;
    	HttpClientHelper clientHelper = new HttpClientHelper();
    	StringBuffer empCodes = new StringBuffer();
		String menuURLPrefix = appConfig("GlobalConfig","PerURLPrefix");
		DataBox itemIsEmpBox = getService().findEmpItemBox("EMP_ID", new DataParam());
		
    	String empIds = param.getString("ids");
    	String empState = param.getString("EMP_STATE");
    	
		String[] idArray = empIds.split(",");
		
		for (String empId : idArray) {
			if(itemIsEmpBox.getDataSet(empId).size() > 0) {
				String empCode = itemIsEmpBox.getDataSet(empId).getDataRow(0).getString("EMP_CODE");
				
				if(StringUtil.isNullOrEmpty(empCodes.toString())) {
					empCodes.append("'"+empCode+"'");
				}
				
				if(!empCodes.toString().contains(empCode)) {
					empCodes.append(",'"+empCode+"'");
				}
			}
		}
		
		responseBody = clientHelper.retrieveGetReponseText(menuURLPrefix+"?code="+empCodes+"&isDisp="+param.getString("isDisp")+"&delTag="+("unapproved".equals(empState) ? "Y" : "N"));
		
		return new AjaxRenderer(responseBody);
	}
    
    public ViewRenderer doApproveRequestAction(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY + "=approve&comeFrome=approve");
	}
    
    
    protected void initParameters(DataParam param) {
    	initParamItem(param, "EMP_STATE", "");
        initParamItem(param, "empSex", "");
        initParamItem(param, "empName", "");
    }
    

    protected HrEmployeeManage getService() {
        return (HrEmployeeManage) this.lookupService(this.getServiceId());
    }
    @PageAction
	public ViewRenderer revokeApproval(DataParam param) {
		String empIds = param.get("ids");
		
		List<DataParam> updateParam = new ArrayList<>();
		DataParam dataParam = null;
		String[] idArray = empIds.split(",");
		for (String empId : idArray) {
			dataParam = new DataParam(); 
 			dataParam.put("EMP_ID",empId);
			updateParam.add(dataParam);
		}
		
		getService().revokeApprovalRecords(updateParam);
		return prepareDisplay(param);
	}
    
    private String appConfig(String section,String entryCode) {
    	BeanFactory beanFactory = BeanFactory.instance();
		AppConfig appConfig = beanFactory.getAppConfig();
    	return appConfig.getConfig(section,entryCode);
    }
    private FormSelect buildEmpWorkStateSelect(){
    	FormSelect formSelect = new FormSelect();
    	formSelect.addValue("00", "在职");
    	formSelect.addValue("01", "离职");
    	formSelect.addHasBlankValue(false);
    	return formSelect;
    }
    
    @Validated
    public ViewRenderer doDeleteAction(DataParam param){
		storeParam(param);
		String empIds = param.get("ids");
		List<DataParam> deleteParam = new ArrayList<>();
		DataParam dataParam = null;
		String[] idArray = empIds.split(",");
		for (String empId : idArray) {
			dataParam = new DataParam(); 
 			dataParam.put("EMP_ID",empId);
 			deleteParam.add(dataParam);
		}
		getService().deleteClusterRecords(deleteParam);	
		return new RedirectRenderer(getHandlerURL(getClass()));
	}
}
