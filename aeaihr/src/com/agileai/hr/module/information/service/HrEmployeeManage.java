package com.agileai.hr.module.information.service;

import java.util.List;

import com.agileai.domain.DataBox;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface HrEmployeeManage
        extends MasterSubService {
	String approveRecord(DataParam param);
	void revokeApprovalRecord(String empId);
	void revokeApprovalRecords(List<DataParam> paramList);
	DataRow getSalaryLimitRecord();
	void updateSalaryLimitRecord(DataParam param);
	DataRow getFulltimeAwardRecord();
	void updateFulltimeAwardRecord(DataParam param);
	DataBox findEmpItemBox(String indexFieldName, DataParam param);
	void deleteClusterRecords(List<DataParam> paramList);
}
